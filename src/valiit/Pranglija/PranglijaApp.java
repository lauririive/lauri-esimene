package valiit.Pranglija;

import java.util.Scanner;

public class PranglijaApp {

        private static final int GUESS_COUNT = 2;
        private static final int MAX_NUMBER = 5;
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

            System.out.println("------------------------------------");
            System.out.println("PRANGLIJA ÄPP");
            System.out.println("------------------------------------");

            // Mäng...
            // 01.01.1970 00:00:00 - ajaarvamise algus.
            long startTime = System.currentTimeMillis();

            // Must maagia
            int count = 0;
            for (; count < GUESS_COUNT; count++) {
                int number1 = (int) (Math.random() * MAX_NUMBER + 1);
                int number2 = (int) (Math.random() * MAX_NUMBER + 1);
                System.out.print(String.format("%d + %d = ", number1, number2));
                int userGuess = Integer.parseInt(scanner.nextLine());
                if (number1 + number2 != userGuess) {
                    // Kasutaja eksis liitmisel...
                    break;
                }
            }

            if (count == GUESS_COUNT) {
                System.out.println("Tubli! Sa võitsid!");

                long endTime = System.currentTimeMillis();
                double elapsedTime = endTime - startTime;
                elapsedTime = elapsedTime / 1000.0;
                System.out.println("Mäng kestis " + elapsedTime + " sekundit.");
            } else {
                System.out.println("Paha lugu :(");
            }



            System.out.println("------------------------------------");
            System.out.println("Copyright: Marek Lints (all rights reserved)");
            System.out.println("------------------------------------");
        }
    }

