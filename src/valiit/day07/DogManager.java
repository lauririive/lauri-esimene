package valiit.day07;

public class DogManager {
    public static void main(String[] args) {
        System.out.println("Greek dog");
        Dog greekDog = new GreekDog("Aphrodite");
        greekDog.bark();

        System.out.println("Latvian Dog");
        Dog latvianDog = new LatvianDog("Rufus");
        latvianDog.bark();

        System.out.println("Serbian Dog");
        Dog serbianDog = new SerbianDog("rex");
        serbianDog.bark();
    }
}
