package valiit.day06;

import java.util.Arrays;

public class MethodDemo {
    public static void main(String[] args) {

        System.out.println("Ex 1");
        test(3);

        System.out.println("Ex 2");
        test2("tere", "head aega");

        System.out.println("Ex 3");
        System.out.println(addVat(89.0));

        System.out.println("Ex 4");
        int[] myEx4Array = composeArray(4, 5, false);
        System.out.println(Arrays.toString(myEx4Array));

        System.out.println("Ex 5");
        printTere();

        System.out.println("Ex 6");
        String gender = deriveGender("49403136526");
        System.out.println(gender);

//        String personalCode1 = "49403136526";
        String personalCode1 = "34501234215";
        int birthYear1 = retrieveBirthYear(personalCode1);
        System.out.printf("Isikukood: %s, sünniaasta: %s", personalCode1, birthYear1);
    }

    private static boolean test(int a) {
        return true;
    }

    private static void test2(String text1, String text2) {
    }

    private static double addVat(double netPrice) {
        return 1.2 * netPrice;
    }

    private static int[] composeArray(int number1, int number2, boolean duplicate) {
        if (duplicate) {
            return new int[]{number1 * 2, number2 * 2};
        } else {
            return new int[]{number1, number2};
        }
    }

    private static void printTere() {
        System.out.println("Tere");
    }

    private static String deriveGender(String personalCode) { // "49403136526" --> "F"
        int firstDigit = Integer.parseInt(personalCode.substring(0, 1));
        return firstDigit % 2 == 1 ? "M" : "F";
//        String gender;
//        if (firstDigit % 2 == 1) {
//            gender = "M";
//        } else {
//            gender = "F";
//        }
//        return gender;
    }

    public static int retrieveBirthYear(String personalCode) {  // "49403136526"
        // .substring()
        // Integer.parseInt()

        // Valideerimine
        if (personalCode == null) {
            return -1;
        } else if (personalCode.length() != 11) {
            return -1;
        }

        int century = Integer.parseInt(personalCode.substring(0, 1)); // 4
        int birthYear = Integer.parseInt(personalCode.substring(1, 3)); // 94

        switch (century) {
            case 1:
            case 2:
                return 1800 + birthYear;
            case 3:
            case 4:
                return 1900 + birthYear;
            case 5:
            case 6:
                return 2000 + birthYear;
            case 7:
            case 8:
                return 2100 + birthYear;
            default:
                return -1;
        }
    }
}
