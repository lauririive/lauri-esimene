package valiit.day04;

public class LoopExercises {

    public static void main(String[] args) {

        System.out.println("Ex 11");
        int number = 1;
        while (number <= 100) {
            System.out.println(number);
            number += 1;
        }

        System.out.println("Ex 12");
        for (int i = 1; i < 101; i++) {
            System.out.println(i);
        }

        System.out.println("Ex 13");
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int i : numbers) {
            System.out.println(i);
        }

        System.out.println("Ex 14");

        System.out.println("Ex 14: option 1");
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }

        System.out.println("Ex 14: option 2");
        for (int i = 3; i <= 99; i = i + 3) {
            System.out.println(i);
        }

        System.out.println("Ex 15");
        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
        String bandsText = "\"";
        for (int i = 0; i < bands.length; i++) {
            bandsText = bandsText + bands[i];
            if (i < bands.length - 1) {
                bandsText = bandsText + ", ";
            }
        }
        bandsText = bandsText + "\"";
        System.out.println(bandsText); // Sun, Metsatöll, Queen, Metallica

        System.out.println("Ex 15: alternative");
        String bandsTextWithJoin = String.join(", ", bands);
        System.out.println(bandsTextWithJoin);

        System.out.println("Ex 16");
        String bandsReverseText = "";
        for (int i = bands.length - 1; i >= 0; i--) {
            bandsReverseText += bands[i];
            if (i > 0) {
                bandsReverseText += ", ";
            }
        }
        System.out.println(bandsReverseText);

        System.out.println("Ex 17");
        // 4 5 8 1 --> neli viis kaheksa üks
        // Samm 1: printige kõik numbrid lihtsalt välja.
        // Samm 2: printige kõik nujbrid sõnana välja.
        // Samm 3: pane sõnad komaga eraldatud lausesse.
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "1":
                    System.out.println("üks");
                    break;
                case "2":
                    System.out.println("kaks");
                    break;
                case "3":
                    System.out.println("kolm");
                    break;
                case "4":
                    System.out.println("neli");
                    break;
                case "5":
                    System.out.println("viis");
                    break;
                case "6":
                    System.out.println("kuus");
                    break;
                case "7":
                    System.out.println("seitse");
                    break;
                case "8":
                    System.out.println("kaheksa");
                    break;
                case "9":
                    System.out.println("üheksa");
                    break;
                case "0":
                    System.out.println("null");
                    break;
            }
        }
    }
}
