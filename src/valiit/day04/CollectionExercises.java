package valiit.day04;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectionExercises {
    public static void main(String[] args) {
        System.out.println("ex 18");

        List<String> myList = new ArrayList<>();
        myList.add("Tallinn");
        myList.add("Tartu");
        myList.add("Võru");
        myList.add("Elva");
        myList.add("Tapa");
        System.out.println("Esimene: " + myList.get(0));

        System.out.println("ex 22");
        Map<String,String[]> countries = new HashMap<>();
        String[] estoniaCities = {"Tallinn", "Tartu", "Valga", "Võru"};
        countries.put("Estonia", estoniaCities);
        String[] swedishCities= {"Stockholm", "Uppsala", "Lund", "Köpling"};
        countries.put("Sweden", swedishCities);
        countries.put("Finland", new String[]{"Helsinki", "Espoo" });
        System.out.println(countries);
        for (String country : countries.keySet()){
        System.out.println("Country" + country);
        System.out.println("Cities");
        for(String city : countries.get(country)){
            System.out.println("\t"+ city);
        }

        }

    }
}
