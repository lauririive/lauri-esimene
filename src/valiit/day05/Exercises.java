package valiit.day05;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Exercises {
    public static void main(String[] args) {
        System.out.println("Ex 1");

        for (int col = 1; col <= 6; col++) {
            if (col <= 7 - 1) {
                System.out.print("#");
            }
        }
        System.out.println();
        System.out.println("ex 2");
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int col = 1; col <= 6; col++) {
            if (col <= 7 - 2) {
                System.out.print("#");
            }
        }
        System.out.println();
        System.out.println("ex 2");
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int col = 1; col <= 6; col++) {
            if (col <= 7 - 3) {
                System.out.print("#");
            }
        }
        System.out.println();
        System.out.println("ex 2");
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int col = 1; col <= 6; col++) {
            if (col <= 7 - 4) {
                System.out.print("#");
            }
        }
        System.out.println();
        System.out.println("ex 2");
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int col = 1; col <= 6; col++) {
            if (col <= 7 - 5) {
                System.out.print("#");
            }
        }
        System.out.println();
        System.out.println("ex 2");
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int col = 1; col <= 6; col++) {
            if (col <= 7 - 6) {
                System.out.print("#");
            }
        }
        System.out.println();
        System.out.println("ex 2");
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
            System.out.println("ex 3");
            for (row = 1; row <= 5; row++) {
                for (int col = 1; col <= 5; col++) {
                    System.out.print(col <= 5 - row ? " " : "@");
                }
                System.out.println();

                System.out.println("ex 4");

            }
        }
        int number = 1234567;
        String numberString = String.valueOf(number);
        String reversedNumberString = "";
        for (char c : numberString.toCharArray()) {
            reversedNumberString = c + reversedNumberString;
        }
        int result = Integer.parseInt(reversedNumberString);
        System.out.println(reversedNumberString);

        System.out.println("ex 5");

        String studentName = args[0];
        int studentPoints = Integer.parseInt(args[1]);
        int grade = 0;

        if (studentPoints < 51) {
            grade = 0;
        } else if (studentPoints < 61) {
            grade = 1;
        } else if (studentPoints > 71) {
            grade = 2;
        } else if (studentPoints > 81) {
            grade = 3;
        } else if (studentPoints > 91) {
            grade = 4;
        } else if (studentPoints > 101) {
            grade = 5;
        } else {
            grade = -1;
        }
        if (grade == 0) {
            System.out.println(String.format("%s: FAIL", studentName));
        } else if (grade > 0) {
            System.out.println(String.format("%s PASS - %d, %d", studentName, grade, studentPoints));
        } else {
            System.out.println("Viga: ebakorrektne sisend");
        }
        System.out.println("ex 7");

        //       Estonia, Tallinn, Jüri Ratas
        //Latvia, Riga, Arturs Krišjānis Kariņš
        //Lithuania, Vilnius, Saulius Skvernelis
        //Finland, Helsinki, Sanna Marin
        //Sweden, Stockholm, Stefan Löfven
        //Norway, Oslo, Erna Solberg
        //Denmark, Copenhagen, Mette Frederiksen
        //Russia, Moscow, Mikhail Mishustin
        //Germany, Berlin, Angela Merkel
        //France, Paris, Édouard Philippe

        String[][] countries = {
                {"Estonia", "Tallinn ", "Jüri Ratas" },
                {"Latvia ", "Riga ", "Arturs Krišjānis Kariņš" },
                {"Lithuania ", "Vilnius ", "Saulius Skvernelis " },
                {"Finland ", "Helsinki ", "Sanna Marin " },
                {"Sweden ", "Stockholm ", "Stefan Löfven " },
                {"Norway ", "Oslo ", "Erna Solberg " },
                {"Denmark ", "Copenhagen ", "Mette Frederiksen " },
                {"Russia ", "Moscow ", "Mikhail Mishustin " },
                {"Germany ", "Berlin ", "Angela Merkel " },
                {"France ", "Paris ", "Édouard Philippe " }


        };
        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i][2]);
        }
        for (int i = 0; i < countries.length; i++) {
            System.out.println(String.format("Country: %s, Capital %s, Prime Minister %s",
                    countries[i][0], countries[i][1], countries[i][2]));

            System.out.println("ex 8");
            String[][][] countries2 = {
                    {{"Estonia" }, {"Tallinn " }, {"Jüri Ratas" }, {"estonian", "russian", "finnish" }},
                    {{"Latvia " }, {"Riga " }, {"Arturs Krišjānis Kariņš" }, {"estonian", "russian", "finnish" }},
                    {{"Lithuania " }, {"Vilnius " }, {"Saulius Skvernelis " }, {"estonian", "russian", "finnish" }},
                    {{"Finland " }, {"Helsinki " }, {"Sanna Marin " }, {"estonian", "russian", "finnish" }},
                    {{"Sweden " }, {"Stockholm " }, {"Stefan Löfven " }, {"estonian", "russian", "finnish" }},
                    {{"Norway " }, {"Oslo " }, {"Erna Solberg " }, {"estonian", "russian", "finnish" }},
                    {{"Denmark " }, {"Copenhagen " }, {"Mette Frederiksen " }, {"estonian", "russian", "finnish" }},
                    {{"Russia " }, {"Moscow " }, {"Mikhail Mishustin " }, {"estonian", "russian", "finnish" }},
                    {{"Germany " }, {"Berlin " }, {"Angela Merkel " }, {"estonian", "russian", "finnish" }},
                    {{"France " }, {"Paris " }, {"Édouard Philippe " }, {"estonian", "russian", "finnish" }}
            };

            for (String[][] country : countries2) {
                String countryName = country[0][0];
                String countryCapital = country[1][0];
                String countryPrimeMinister = country[2][0];
                String[] countryLanguages = country[3];
                System.out.println(String.format("%s/%s/%s:",
                        country[0][0], country[1][0], country[2][0]));
                for (String countryLanguage : countryLanguages) {
                    System.out.println("\t\t" + countryLanguage);
                }
                System.out.println("Ex 9");
            }
        }
        List<List<List<String>>> countries3 =
                Arrays.asList( // riigid
                        Arrays.asList( // riik
                                Collections.singletonList("Estonia"), // riigi nimi
                                Collections.singletonList("Tallinn"), // riigi pealinn
                                Collections.singletonList("Jüri Ratas"), // riigi peaminister
                                Arrays.asList("Estonian", "Russian", "Finnish") // riigi keeled
                        ),
                        Arrays.asList(
                                Collections.singletonList("Latvia"),
                                Collections.singletonList("Riga"),
                                Collections.singletonList("Arturs Krišjānis Kariņš"),
                                Arrays.asList("Latvian", "Russian", "Estonian")
                        ),
                        Arrays.asList(
                                Collections.singletonList("Lithuania"),
                                Collections.singletonList("Vilnius"),
                                Collections.singletonList("Saulius Skvernelis"),
                                Arrays.asList("Lithuanian", "Polish", "Russian")
                        )
                );

        int i;
        for (i = 0; i < countries3.size(); i++) {
            String countryName = countries3.get(i).get(0).get(0);
            String countryCapital = countries3.get(i).get(1).get(0);
            String countryPrimeMinister = countries3.get(i).get(2).get(0);
            List<String> countryLanguages = countries3.get(i).get(3);

//            System.out.println(String.format());
            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
            for (int j = 0; j < countryLanguages.size(); j++) {
                System.out.println("\t\t" + countryLanguages.get(j));
            }
        }
    }
}