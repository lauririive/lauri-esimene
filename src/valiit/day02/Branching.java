package valiit.day02;

public class Branching {
    public static void main(String[] args) {
        boolean isValid = true;
        if (isValid) {

        }
//        else if (anotherValueIsValid) {
//
//        } else if (anotherValueIsValid2) {
//
//        } else if (anotherValueIsValid3) {
//
//        } else {
//
//        }


        int someNumber = 13;

        // Tingimustehe (inline-if statement)
        String canDivideByThree = someNumber % 3 == 0 ? "jagub kolmega" : "ei jagu kolmega";
        System.out.println(canDivideByThree);

        // klassikaline if-else statement
        if (someNumber % 3 == 0) {
            canDivideByThree = "jagub kolmega";
        } else {
            canDivideByThree = "ei jagu kolmega";
        }
        System.out.println(canDivideByThree);

        int aNumber = 5110026;
        String canDivideByTwelve;
        if (aNumber % 12 == 0) {
            canDivideByTwelve = "jagub kaheteistkümnega";
        } else {
            canDivideByTwelve = "ei jagu kaheteistkümnega";
        }
        System.out.println(canDivideByTwelve);

        // Switch statement
        // Kasulik kasutada siis, kui
        // a) kui valikuvariante on kindel kogus
        // b) kui hargnemisi on palju
        String trafficLightColor = "green";

        switch (trafficLightColor) {
            case "red":
                System.out.println("Cars must stop and wait.");
                // Another statements...
                break;
            case "yellow":
                System.out.println("Cars must slow down...");
                break;
            case "green":
                System.out.println("Cars can drive normally.");
                break;
            default:
                System.out.println("Help! Traffic lights are broken!");
        }
        String trafficLight = "green";
        switch (trafficLight) {
            case "red":
                System.out.println("Ära sõida!");
                break;
            case "yellow":
                System.out.println("Varsti");
                break;
            case "green":
                System.out.println("Sõida!");
                break;
            default:
                System.out.println("Mingi jama");
        }


    }
}
